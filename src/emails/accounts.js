const sgMail = require('@sendgrid/mail')

//const sendgridAPIKey = 'SG.N6SJH-vZQZavOwzvv99VTg.gKSOgb94auAi6mnp_-S4JXiYbUuwieeX_F794Y9uuQc'
//sgMail.setApiKey(sendgridAPIKey)

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'roniedias76@gmail.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the app, ${name}. Let me know how you get along with the app.`
    })
}

const sendCancelationEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'roniedias76@gmail.com',
        subject: 'Sorry to see you go!',
        text: `Goodbeye, ${name}. I hope to see you back sometime soon`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelationEmail
}

// sgMail.send({
//     to: 'roniedias76@gmail.com',
//     from: 'roniedias76@gmail.com',
//     subject: 'This is my first creation',
//     text: 'I hope this one actually get to you.'
// }).then(() => {
// }).catch((e) => {
//     console.log(e)
// })